/**
 * 
 */
package de.tarent.commons.ui.swt;



import net.miginfocom.swt.MigLayout;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import de.tarent.commons.ui.I18n;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class AboutDialog extends Dialog {
	
	protected Image productLogo;
	protected String productName;
	protected String productDesc;
	protected String productVersion;
	protected String serverVersion;
	protected String build;
	protected String server;
	protected String database;
	protected String contributionsFile;
	protected String licenseFile;
	protected I18n messages;


	public static void main(String[] args) {
		Display display = new Display();
		Display.setAppName("kaliko");
		AboutDialog eventDialog = new AboutDialog(null, null, "kaliko", "A webservice based Calendar", "0.6.0-SNAPSHOT", "0.6.0-SNAPSHOT", "dev", "server", "database", null, null);

		eventDialog.open();

		display.dispose();
	}

	public AboutDialog(Shell parent, Image productLogo, String productName, String productDesc, String productVersion, String serverVersion, String build, String server, String database, String contributionsFile, String licenseFile) {
		super(parent);
		
		this.productLogo = productLogo;
		this.productName = productName;
		this.productDesc = productDesc;
		this.productVersion = productVersion;
		this.serverVersion = serverVersion;
		this.build = build;
		this.server = server;
		this.database = database;
		this.contributionsFile = contributionsFile;
		this.licenseFile = licenseFile;
		this.messages = new I18n("de.tarent.commons.ui.messages.GUI");
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new MigLayout());
		
		Label name = new Label(composite, SWT.NONE);
		name.setText(productName);
		
		Label version = new Label(composite, SWT.NONE);
		version.setText(productVersion);
		
		return composite;
	}

	/**
	 * @see org.eclipse.jface.dialogs.Dialog#createButton(org.eclipse.swt.widgets.Composite, int, java.lang.String, boolean)
	 */
	@Override
	protected Button createButton(Composite composite, int id, String arg2, boolean arg3) {
		Button button = super.createButton(composite, id, arg2, arg3);
		
		if(id == IDialogConstants.OK_ID)
			button.setVisible(false);
		else if(id == IDialogConstants.CANCEL_ID)
			button.setText(messages.getString("About_Close"));
		
		return button;
	}
	
	
}
