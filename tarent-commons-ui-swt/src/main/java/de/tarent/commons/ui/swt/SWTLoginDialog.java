/**
 * 
 */
package de.tarent.commons.ui.swt;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.tarent.commons.ui.ConnectionParameters;
import de.tarent.commons.ui.I18n;
import de.tarent.commons.ui.LoginDialog;
import de.tarent.commons.ui.swt.connection.ConnectionPropertiesViewer;

/**
 * 
 * A SWT-implementation of a common Login-Dialog
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class SWTLoginDialog implements LoginDialog {
	
	protected Label connectionLabel;
	protected Label userLabel;
	protected Label passwordLabel;
	
	protected Combo connectionCombo;
	
	protected Text userText;
	protected Text passwordText;
	
	protected Composite panel;
	
	protected Button submitButton;
	protected Button cancelButton;
	protected Button connectionsButton;
	
	protected ProgressBar progressBar;
	
	protected Shell shell;
	
	protected Collection<ConnectionParameters> connectionParameters;
	
	protected List<ActionListener> actionListeners;
	
	protected boolean currentlyLoggingIn;
	
	protected String user;
	protected String password;
	protected ConnectionParameters selectedConnection;
	
	protected I18n messages;
	
	public SWTLoginDialog(Display display, Image[] windowIcons, Collection<ConnectionParameters> connections, String defaultConnection, String defaultUserName, String defaultPassword) {
		
		this.messages = new I18n("de.tarent.commons.ui.messages.GUI");
		
		this.connectionParameters = connections;
		
		shell = new Shell(display);
		shell.setText("Login - Kaliko");
		
		if(windowIcons != null)
			shell.setImages(windowIcons);
		shell.setLayout(new MigLayout("","20[left,fill]10[fill,grow]20","20 []10[]10[]10[]10[]20"));
		connectionLabel = new Label(shell, SWT.NONE);
		connectionLabel.setLayoutData("sg labels");
		connectionLabel.setText("Connection:");
		
		connectionCombo = new Combo(shell, SWT.READ_ONLY);
		connectionCombo.setLayoutData("wrap");
		connectionCombo.setItems(getConnectionNames(connections));
		
		boolean set = false;
		
		for(int i=0; i < connectionCombo.getItemCount(); i++)
			if(connectionCombo.getItem(i).equals(defaultConnection)) {
				connectionCombo.select(i);
				set = true;
				break;
			}
		
		// If no connection has been select yet just select the first one.
		if(!set && connectionCombo.getItemCount() > 0)
			connectionCombo.select(0);
		
		userLabel = new Label(shell, SWT.NONE);
		userLabel.setLayoutData("sg labels");
		userLabel.setText("User:");
		
		userText = new Text(shell, SWT.BORDER);
		userText.setLayoutData("wrap");
				
		if(defaultUserName != null)
			userText.setText(defaultUserName);
		
		userText.forceFocus();
		
		passwordLabel = new Label(shell, SWT.NONE);
		passwordLabel.setLayoutData("sg labels");
		passwordLabel.setText("Password:");
		
		passwordText = new Text(shell, SWT.PASSWORD | SWT.BORDER);
		passwordText.setLayoutData("wrap");
		
		if(defaultPassword != null)
			passwordText.setText(defaultPassword);
		
		progressBar = new ProgressBar(shell, SWT.HORIZONTAL | SWT.SMOOTH);
		progressBar.setLayoutData("span, wrap");
		progressBar.setMaximum(100);
		progressBar.setVisible(false);
		
		panel=new Composite(shell,SWT.NONE);
		panel.setLayout(new MigLayout("","0[][left][grow, right]0"));
		panel.setLayoutData("span 2, grow");
		
		LoginDialogSelectionListener listener = new LoginDialogSelectionListener();
		submitButton = new Button(panel,SWT.NONE);
		submitButton.setText(messages.getString("LoginDialog_Button_Ok"));
		submitButton.setLayoutData("width 200");
		submitButton.addSelectionListener(listener);
		
				
		connectionsButton = new Button(panel, SWT.NONE);
		connectionsButton.setText(messages.getString("LoginDialog_Button_ConnectionProperties"));
		connectionsButton.addSelectionListener(listener);
		
		cancelButton = new Button(panel, SWT.NONE);
		cancelButton.setText(messages.getString("LoginDialog_Button_Cancel"));
		cancelButton.addSelectionListener(listener);
		
		
		shell.setDefaultButton(submitButton);
		
		shell.pack();
		
		/* center shell on screen */
	
        Rectangle displayBounds = shell.getDisplay().getBounds();
       
        int nWidth = displayBounds.width  / 2;
        int nHeight = displayBounds.height / 2;

        int nLeft = nWidth - (shell.getBounds().width / 2);
        int nTop = nHeight- (shell.getBounds().height / 2);
                
        // Set shell bounds,
        shell.setLocation(nLeft, nTop);
	}
	
	protected String[] getConnectionNames(Collection<ConnectionParameters> connections) {
		String[] connectionNames = new String[connections.size()];
		
		Iterator<ConnectionParameters> it = connections.iterator();
		
		int i=0;
		
		while(it.hasNext()) {
			connectionNames[i] = it.next().getName();
			i++;
		}
		
		return connectionNames;
	}
	
	public Collection<ConnectionParameters> getAvailableConnections() {
		return connectionParameters;
	}

	public Shell getShell() {
		return shell;
	}

	/**
	 * @see de.tarent.commons.ui.DeferringLoginProvider#addActionListener(java.awt.event.ActionListener)
	 */
	public void addActionListener(ActionListener listener) {
		getActionListeners().add(listener);
	}

	/**
	 * @see de.tarent.commons.ui.DeferringLoginProvider#removeActionListener(java.awt.event.ActionListener)
	 */
	public void removeActionListener(ActionListener listener) {
		getActionListeners().remove(listener);
	}

	/**
	 * @see de.tarent.commons.ui.DeferringLoginProvider#reset()
	 */
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see de.tarent.commons.ui.DeferringLoginProvider#setDialogVisible(boolean)
	 */
	public void setDialogVisible(boolean show) {
		if(show)
			shell.open();
		else
			shell.close();
	}

	/**
	 * @see de.tarent.commons.ui.DeferringLoginProvider#setSessionContinuable(boolean)
	 */
	public void setSessionContinuable(boolean arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see de.tarent.commons.ui.DeferringLoginProvider#setStatus(int)
	 */
	public void setStatus(final int status) {
		getShell().getDisplay().asyncExec(new Runnable() {

			/**
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				if(!progressBar.isDisposed())
					progressBar.setSelection(status);
			}
			
		});
	}

	/**
	 * @see de.tarent.commons.ui.DeferringLoginProvider#setStatusText(java.lang.String)
	 */
	public void setStatusText(final String text) {
		getShell().getDisplay().asyncExec(new Runnable() {

			/**
			 * @see java.lang.Runnable#run()
			 */
			public void run() {
				if(!progressBar.isDisposed())
					progressBar.setToolTipText(text);
			}
			
		});
	}

	public ConnectionParameters getSelectedConnection() {
		return selectedConnection;
	}

	/**
	 * @see de.tarent.commons.ui.LoginProvider#getPassword()
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @see de.tarent.commons.ui.LoginProvider#getUserName()
	 */
	public String getUserName() {
		return user;
	}

	/**
	 * @see de.tarent.commons.ui.LoginProvider#shouldContinueSession()
	 */
	public boolean shouldContinueSession() {
		// TODO implement
		return false;
	}
	
	protected void fireActionEvent(final String actionCommand)	{
		final Iterator it = getActionListeners().iterator();

		while(it.hasNext())
			((ActionListener)it.next()).actionPerformed(new ActionEvent(this, -1, actionCommand));
	}
	
	protected void fireLoginRequested()	{
		fireActionEvent("login");
	}

	protected void fireCancelRequested() {
		fireActionEvent("cancel");
	}
	
	protected void fireQuitRequested() {
		fireActionEvent("quit");
	}

	protected void fireSelectedConnectionChanged() {
		fireActionEvent("connection_changed");
	}
	
	protected List<ActionListener> getActionListeners() {
		if(actionListeners == null)
			actionListeners = new ArrayList<ActionListener>();

		return actionListeners;
	}
	
	protected void setControlsEnabled(boolean enabled) {
		submitButton.setEnabled(enabled);
		connectionsButton.setEnabled(enabled);
		connectionCombo.setEnabled(enabled);
		connectionLabel.setEnabled(enabled);
		
		userText.setEnabled(enabled);
		userLabel.setEnabled(enabled);
		passwordText.setEnabled(enabled);
		passwordLabel.setEnabled(enabled);
	}

	
	protected class LoginDialogSelectionListener extends SelectionAdapter {
		/**
		 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
		 */
		@Override
		public void widgetSelected(SelectionEvent event) {
			if(event.getSource().equals(cancelButton)) {
				if(currentlyLoggingIn)
					fireCancelRequested();
				else
					fireQuitRequested();
			} else if(event.getSource().equals(submitButton)) {
				// TODO: Check if a connection exists and is selected
				
				setControlsEnabled(false);
				user = userText.getText();
				password = passwordText.getText();
				selectedConnection = (ConnectionParameters)connectionParameters.toArray()[connectionCombo.getSelectionIndex()];
				
				progressBar.setVisible(true);
				shell.pack();
				cancelButton.setText(messages.getString("LoginDialog_Button_Cancel"));
				cancelButton.setToolTipText(messages.getString("LoginDialog_Button_Cancel_ToolTip"));
				currentlyLoggingIn = true;
				fireLoginRequested();
				
			} else if(event.getSource().equals(connectionCombo)) {
				fireSelectedConnectionChanged();
			} else if(event.getSource().equals(connectionsButton)) {
				ConnectionPropertiesViewer viewer = new ConnectionPropertiesViewer(SWTLoginDialog.this.getShell(), getAvailableConnections());
				viewer.getShell().open();
			}
			
		}
	}
}
