package de.tarent.commons.ui.swt.connection;


/**
 * @author Dennis Schall (d.schall@tarent.de) tarent GmbH Bonn
 */


import java.awt.event.ActionEvent;

import javax.swing.Action;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;

import de.tarent.commons.ui.ConnectionParameters;
import de.tarent.commons.ui.I18n;

public class ConnectionPropertiesEditor {
	protected Shell parent;
	protected Shell shell;

	protected Label nameLabel;
	protected Label addressLabel;
	protected Label portLabel;
	protected Label moduleLabel;
	protected Label tlsLabel;

	protected Text nameText;
	protected Text addressText;
	protected Spinner portSpinner;
	protected Text moduleText;
	protected Button tlsCheck;

	protected Button submitButton;
	protected Button cancelButton;

	protected Composite buttonPanel;

	protected I18n messages;

	protected Action submitAction;
	
	public ConnectionPropertiesEditor(Shell parent) {
		this(parent, "", false, "", 80, "");
	}
	
	public ConnectionPropertiesEditor(Shell parent, ConnectionParameters connection) {
		this(parent, connection.getName(), connection.useTLS(), connection.getAddress(), connection.getPort(), connection.getModule());
	}

	public ConnectionPropertiesEditor(Shell parent, String name, boolean tlsSecured, String address, int port, String module) {

		this.messages = new I18n("de.tarent.commons.ui.messages.GUI");

		this.parent = parent;

		shell = new Shell(parent, SWT.APPLICATION_MODAL|SWT.DIALOG_TRIM);
		shell.setText(messages.getString("ConnectionPropertiesEditor_Title"));
		shell.setLayout(new MigLayout("","20[left,fill]10[grow,fill]20","20[]10[]10[]10[]20"));

		nameLabel = new Label(shell, SWT.NONE);
		nameLabel.setText(messages.getString("ConnectionPropertiesEditor_ConnectionLabel"));

		nameText = new Text(shell, SWT.SINGLE | SWT.BORDER);
		nameText.setLayoutData("wmin 400,wrap");
		nameText.setText(name);

		addressLabel = new Label(shell, SWT.NONE);
		addressLabel.setText(messages.getString("ConnectionPropertiesEditor_ServerURL"));

		addressText = new Text(shell, SWT.SEARCH | SWT.BORDER);
		addressText.setLayoutData("wmin 400, wrap");
		addressText.setText(address);
		addressText.setMessage(messages.getString("ConnectionPropertiesEditor_ServerURLHint"));
		addressText.setToolTipText(messages.getString("ConnectionPropertiesEditor_ServerURLHint"));

		portLabel = new Label(shell, SWT.NONE);
		portLabel.setText(messages.getString("ConnectionPropertiesEditor_ServerPort"));

		portSpinner = new Spinner(shell, SWT.NONE | SWT.BORDER);
		portSpinner.setLayoutData("wmin 400, wrap");
		portSpinner.setMinimum(0);
		portSpinner.setMaximum(65535);
		portSpinner.setSelection(port);

		moduleLabel = new Label(shell, SWT.NONE);
		moduleLabel.setText(messages.getString("ConnectionPropertiesEditor_Module"));

		moduleText = new Text(shell, SWT.SINGLE | SWT.BORDER);
		moduleText.setLayoutData("wmin 400, wrap");
		moduleText.setText(module);
		
		tlsLabel = new Label(shell, SWT.NONE);
		tlsLabel.setText(messages.getString("ConnectionPropertiesEditor_UseSSL"));
		
		tlsCheck = new Button(shell, SWT.CHECK);
		tlsCheck.setLayoutData("wrap");
		tlsCheck.setSelection(tlsSecured);
		

		Listener buttonListener = new Listener() {
			public void handleEvent(Event event) {
				if (event.widget == submitButton){
					if(submitAction != null)
						submitAction.actionPerformed(new ActionEvent(this, 0, "connectionEditSubmit"));
					
					getShell().dispose();
				} else if (event.widget == cancelButton){
					getShell().dispose();
				}

			}
		};

		buttonPanel = new Composite(shell,SWT.NONE);
		buttonPanel.setLayout(new MigLayout("","0[left][grow, right]0"));
		buttonPanel.setLayoutData("span 2");

		submitButton = new Button(buttonPanel,SWT.NONE);
		submitButton.setText(messages.getString("CommonDialogButtons_Submit"));
		submitButton.setToolTipText(messages.getString("CommonDialogButtons_Submit_Tooltip"));
		submitButton.addListener(SWT.Selection, buttonListener);

		cancelButton = new Button(buttonPanel,SWT.NONE);
		cancelButton.setText(messages.getString("CommonDialogButtons_Cancel"));
		cancelButton.setToolTipText(messages.getString("CommonDialogButtons_Cancel_Tooltip"));
		cancelButton.addListener(SWT.Selection,buttonListener);

		shell.setLocation(470,420);

		shell.pack();

		Rectangle displayBounds = shell.getDisplay().getBounds();
	    
	    int nWidth = displayBounds.width  / 2;
	    int nHeight = displayBounds.height / 2;

	    int nLeft = nWidth - (shell.getBounds().width / 2);
	    int nTop = nHeight- (shell.getBounds().height / 2);
	            
	    // Set shell bounds,
	    shell.setLocation(nLeft, nTop);
	}

	public void setSubmitAction(Action submitAction) {
		this.submitAction = submitAction;
	}

	public String getName() {
		return nameText.getText();
	}

	public String getAddress() {
		return addressText.getText();
	}

	public String getModule() {
		return moduleText.getText();
	}

	public int getPort() {
		return portSpinner.getSelection();
	}
	
	public boolean useTLS() {
		return tlsCheck.getSelection();
	}

	public Shell getShell() {
		return shell;
	}

	public void setShell(Shell shell) {
		this.shell = shell;
	}
}