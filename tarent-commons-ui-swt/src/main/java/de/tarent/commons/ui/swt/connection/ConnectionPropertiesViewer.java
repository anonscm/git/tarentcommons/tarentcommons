package de.tarent.commons.ui.swt.connection;


/**
 * @author Dennis Schall (d.schall@tarent.de) tarent GmbH Bonn
 */

import java.awt.event.ActionEvent;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.AbstractAction;

import net.miginfocom.swt.MigLayout;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import de.tarent.commons.ui.ConnectionParameters;
import de.tarent.commons.ui.ConnectionParametersImpl;
import de.tarent.commons.ui.I18n;


public class ConnectionPropertiesViewer<E> {
	protected Display display;
	protected Shell parent;
	protected Shell shell;
	
	protected java.util.List<ConnectionParameters> connections;
	
	protected List connectionList;
	
	protected Label nameLabel;
	protected Label addressLabel;
//	protected Label moduleLabel;
	
	protected Text nameText;
	protected Text addressText;
//	protected Text moduleText;
	
	protected Composite buttonpanel1;
	
	protected Button newButton;
	protected Button editButton;
	protected Button deleteButton;
	
	protected Composite buttonpanel2;
	
	protected Button submitButton;
	protected Button cancelButton;
	
	protected I18n messages;

	public ConnectionPropertiesViewer(Shell parent, Collection<ConnectionParameters> connections) {
		this.messages = new I18n("de.tarent.commons.ui.messages.GUI");
		this.parent = parent;
		this.connections = toList(connections);
		shell = new Shell(parent,SWT.APPLICATION_MODAL|SWT.DIALOG_TRIM);
		shell.setFocus();
		shell.setText(messages.getString("ConnectionPropertiesViewer_Title"));
		shell.setLayout(new MigLayout("","20[grow,fill]10[fill]10[fill,grow]20","20[fill]10[fill]10[fill]10[fill]10[fill]10[]20"));
		
		connectionList = new List(shell, SWT.V_SCROLL | SWT.BORDER);
		connectionList.setLayoutData("top, wmin 380,height 200,hmax 200, spany 5");
		connectionList.addSelectionListener(new SelectionAdapter() {

			/**
			 * @see org.eclipse.swt.events.SelectionAdapter#widgetSelected(org.eclipse.swt.events.SelectionEvent)
			 */
			@Override
			public void widgetSelected(SelectionEvent event) {
				refreshFields();
			}
			
		});
		
		
		Iterator<ConnectionParameters> it = connections.iterator();
		
		while(it.hasNext())
			connectionList.add(it.next().getName());
	

		nameLabel = new Label(shell, SWT.NONE);
		nameLabel.setText(messages.getString("ConnectionPropertiesViewer_ConnectionLabel"));
		nameLabel.setLayoutData("sg labels");
		
		nameText = new Text(shell, SWT.READ_ONLY | SWT.BORDER);
		nameText.setLayoutData("wmin 300,span 2, wrap");
		
		addressLabel = new Label(shell, SWT.NONE);  
		addressLabel.setText(messages.getString("ConnectionPropertiesViewer_ServerURL"));
		addressLabel.setLayoutData("sg labels");
		
		addressText = new Text(shell, SWT.READ_ONLY | SWT.BORDER);
		addressText.setLayoutData("span 2, wrap");
		
		buttonpanel1 = new Composite(shell,SWT.NONE);
		buttonpanel1.setLayout(new MigLayout("","0[left, grow][center,grow][right,grow]0"));
		buttonpanel1.setLayoutData("span 2,wrap");
		
		Listener buttonListener = new Listener() {
			public void handleEvent(Event event) {
				if (event.widget == newButton){
					final ConnectionPropertiesEditor connectionPropertiesEditor= new ConnectionPropertiesEditor(ConnectionPropertiesViewer.this.getShell());
					connectionPropertiesEditor.setSubmitAction(new AbstractAction() {

						/**
						 * 
						 */
						private static final long serialVersionUID = 3858467781704580676L;

						/**
						 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
						 */
						public void actionPerformed(ActionEvent arg0) {
							addConnection(new ConnectionParametersImpl(
									connectionPropertiesEditor.getName(),
									connectionPropertiesEditor.getAddress(),
									connectionPropertiesEditor.getModule(),
									connectionPropertiesEditor.getPort(),
									connectionPropertiesEditor.useTLS()));
						}
					});
					connectionPropertiesEditor.getShell().open();
				} else if (event.widget == editButton){
					final ConnectionPropertiesEditor connectionPropertiesEditor = new ConnectionPropertiesEditor(ConnectionPropertiesViewer.this.getShell(), getSelectedConnection());
					
					connectionPropertiesEditor.setSubmitAction(new AbstractAction() {

						/**
						 * 
						 */
						private static final long serialVersionUID = 3858467781704580676L;

						/**
						 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
						 */
						public void actionPerformed(ActionEvent arg0) {
							getSelectedConnection().setName(connectionPropertiesEditor.getName());
							getSelectedConnection().setAddress(connectionPropertiesEditor.getAddress());
							getSelectedConnection().setModule(connectionPropertiesEditor.getModule());
							getSelectedConnection().setPort(connectionPropertiesEditor.getPort());
							getSelectedConnection().setUseTLS(connectionPropertiesEditor.useTLS());
							
							refreshFields();
						}
					});
					
					connectionPropertiesEditor.getShell().open();
				} else if (event.widget == deleteButton){
					connectionList.remove(connectionList.getSelectionIndex());
				} else if (event.widget == submitButton){
					getShell().dispose();
				} else if (event.widget == cancelButton){
					getShell().dispose();
				}

			}
		};

		newButton = new Button(buttonpanel1,SWT.NONE);
		newButton.setText(messages.getString("ConnectionPropertiesViewer_New"));
		newButton.setToolTipText(messages.getString("ConnectionPropertiesViewer_New_ToolTip"));
		newButton.addListener(SWT.Selection, buttonListener);
		
		
		editButton = new Button(buttonpanel1,SWT.NONE);
		editButton.setText(messages.getString("ConnectionPropertiesViewer_Edit"));
		editButton.setToolTipText(messages.getString("ConnectionPropertiesViewer_Edit_ToolTip"));
		editButton.setLayoutData("");
		editButton.addListener(SWT.Selection, buttonListener);
		
		deleteButton = new Button(buttonpanel1,SWT.NONE);
		deleteButton.setText(messages.getString("ConnectionPropertiesViewer_Delete"));
		deleteButton.setToolTipText(messages.getString("ConnectionPropertiesViewer_Delete_ToolTip"));
		deleteButton.setLayoutData("");
		deleteButton.addListener(SWT.Selection, buttonListener);		
		
		buttonpanel2 = new Composite(shell, SWT.NONE);
		buttonpanel2.setLayout(new MigLayout("","0[left][grow, right]0"));
		buttonpanel2.setLayoutData("cell 0 5 3");
				
		submitButton = new Button(buttonpanel2,SWT.NONE);
		submitButton.setText(messages.getString("CommonDialogButtons_Submit"));
		submitButton.setToolTipText(messages.getString("CommonDialogButtons_Submit_Tooltip"));
		submitButton.addListener(SWT.Selection, buttonListener);
				
		cancelButton = new Button(buttonpanel2,SWT.NONE);
		cancelButton.setText(messages.getString("CommonDialogButtons_Cancel"));
		cancelButton.setToolTipText(messages.getString("CommonDialogButtons_Cancel_Tooltip"));
		cancelButton.addListener(SWT.Selection, buttonListener);
	/*	
		   Rectangle displayBounds = shell.getDisplay().getBounds();
	       
	        int nWidth = displayBounds.width  / 2;
	        int nHeight = displayBounds.height / 2;

	        int nLeft = nWidth - (shell.getBounds().width / 2);
	        int nTop = nHeight- (shell.getBounds().height / 2);
	                */
	        // Set shell bounds,
	        shell.setLocation(340, 390);
		
		
		shell.pack();
		
		Rectangle displayBounds = shell.getDisplay().getBounds();
	       
        int nWidth = displayBounds.width  / 2;
        int nHeight = displayBounds.height / 2;

        int nLeft = nWidth - (shell.getBounds().width / 2);
        int nTop = nHeight- (shell.getBounds().height / 2);
                
        // Set shell bounds,
        shell.setLocation(nLeft, nTop);
		
		refreshFields();
	}
	
	protected void addConnection(ConnectionParameters connection) {
		connections.add(connection);
		
		refreshList();
		
		connectionList.select(connectionList.getItemCount()-1);
		
		refreshFields();
	}
	
	protected ConnectionParameters getSelectedConnection() {
		int index = connectionList.getSelectionIndex();
		
		if(index < 0)
			return null;
		
		return connections.get(index);
	}
	
	protected void refreshList() {
		Iterator<ConnectionParameters> it = connections.iterator();
		
		connectionList.removeAll();
		
		while(it.hasNext())
			connectionList.add(it.next().getName());
	}
	
	protected void refreshFields() {
		
		ConnectionParameters selectedConnection = getSelectedConnection();
		
		if(selectedConnection == null)
			return;

		nameText.setText(selectedConnection.getName());
		try {
			addressText.setText(selectedConnection.toURL().toString());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	public Shell getShell() {
		return shell;
	}


	public void setShell(Shell shell) {
		this.shell = shell;
	}
	
	public Collection<ConnectionParameters> getConnections() {
		return connections;
	}

	public void setConnections(Collection<ConnectionParameters> connections) {
		this.connections = toList(connections);
		
		refreshList();
		refreshFields();
	}
	
	protected java.util.List<ConnectionParameters> toList(Collection<ConnectionParameters> collection) {
		if(collection instanceof List)
			return (java.util.List<ConnectionParameters>)collection;
		
		return new ArrayList<ConnectionParameters>(collection);
	}
}