/**
 * 
 */
package de.tarent.commons.ui;

import java.net.MalformedURLException;
import java.net.URL;


/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface ConnectionParameters {

	public String getName();
	
	public String getAddress();
	
	public String getModule();
	
	public int getPort();
	
	public URL toURL() throws MalformedURLException;
	
	public boolean useTLS();
	
	public void setName(String name);

	public void setAddress(String address);
	
	public void setModule(String module);
	
	public void setPort(int port);
	
	public void setUseTLS(boolean useTLS);
}
