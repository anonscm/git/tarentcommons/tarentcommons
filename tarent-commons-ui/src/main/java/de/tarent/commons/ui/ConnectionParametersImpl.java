/**
 * 
 */
package de.tarent.commons.ui;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class ConnectionParametersImpl implements ConnectionParameters {

	protected String name;
	protected String address;
	protected String module;
	protected int port;
	protected boolean useTLS;
	
	public ConnectionParametersImpl(String name, String address, String module, int port, boolean useTLS) {
		this.name = name;
		this.address = address;
		this.module = module;
		this.port = port;
		this.useTLS = useTLS;
	}
	
	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#getAddress()
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#getModule()
	 */
	public String getModule() {
		return module;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#getPort()
	 */
	public int getPort() {
		return port;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#setAddress(java.lang.String)
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#setModule(java.lang.String)
	 */
	public void setModule(String module) {
		this.module = module;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#setName(java.lang.String)
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#setPort(int)
	 */
	public void setPort(int port) {
		this.port = port;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#setUseTLS(boolean)
	 */
	public void setUseTLS(boolean useTLS) {
		this.useTLS = useTLS; 
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#useTLS()
	 */
	public boolean useTLS() {
		return useTLS;
	}

	/**
	 * @see de.tarent.commons.ui.ConnectionParameters#toURL()
	 */
	public URL toURL() throws MalformedURLException {
		return new URL(useTLS() ? "https" : "http", getAddress(), getPort(), "/" + getModule() + "/soap/kaliko");
	}

}
