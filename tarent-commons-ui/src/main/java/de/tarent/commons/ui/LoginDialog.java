/**
 * 
 */
package de.tarent.commons.ui;

/**
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public interface LoginDialog {

	/**
	 * <p>Returns the username to be used for connection</p>
	 * 
	 * <p>Only used, if shouldContinueSession() returns false</p>
	 * 
	 * @return the username to be used for connection
	 */
	public String getUserName();
	
	/**
	 * <p>Returns the password to be used for connection</p>
	 * 
	 * <p>Only used, if shouldContinueSession() returns false</p>
	 * 
	 * @return
	 */
	public String getPassword();
	
	/**
	 * 
	 * If the application should try to reconnect with an existing session-cookie
	 * 
	 * @return if the user wants to reconnect
	 */
	public boolean shouldContinueSession();
	
	
	/**
	 * 
	 * <p>Returns the selected {@link de.tarent.commons.ui.ConnectionParameters} to be used for connection</p>
	 * 
	 * <p>Only used, if shouldContinueSession() returns false</p>
	 * 
	 * @return the {@link de.tarent.commons.ui.ConnectionParameters} containing connection-information like server-address, port etc
	 */
	public ConnectionParameters getSelectedConnection();
	
	/**
	 * If the class implementing this interface has something like a text-status-information
	 * use this method to show some text to the user
	 * 
	 * @param text the text to be shown
	 */
	public void setStatusText(String tText);
	
	
	/**
	 * If the class implementing this interface has something like a progress-bar
	 * use this method to show the progress (in percent).
	 * 
	 * @param status in percent
	 */
	public void setStatus(int status);
	
	
	/**
	 * If the user-interface should be visible
	 * 
	 * @param pVisible true makes the user-interface visible
	 */
	public void setDialogVisible(boolean pVisible);
	
	/**
	 * Resets the dialog to the initial state.
	 *
	 */
	public void reset();
	
	
	public void setSessionContinuable(boolean pContinuable);
}
