/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.commons.datahandling.binding;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.tarent.commons.datahandling.entity.EntityException;
import de.tarent.commons.datahandling.entity.EntityList;
import de.tarent.commons.utils.Pojo;

/**
 * This class implements a generic compound model that can store hierarchically 
 * organized model structures.
 * 
 * <p>The compoundModel can be used to store different Models in the same data structures, 
 * each can be accessed through a string key while a "." marks the diffeent hierarchical units</p>
 * <p> The easiest way to use a CompoundModel ist to extend it and to add problem specific getters, stters and other methods</p>
 * @author Steffi Tinder, tarent GmbH
 *
 */
public class CompoundModel extends AbstractModel implements DataChangedListener {

	/** Models by theire path (prefix) */
	private HashMap subModels;
	
	/** Stores the prefix of a submodel by the model */
	private HashMap subModelPaths;

	public CompoundModel() {
		subModels = new HashMap();
		subModelPaths = new HashMap();
	}
	
	/**
	 * retrieves an Attribute with regard to a possible hierarchically organized model structure
	 */
	public Object getAttribute(String key) throws EntityException {
		if (subModels.containsKey(key)) return subModels.get(key);
		if (key.indexOf('.')==-1){
			//no dot contained in key
			return subModels.get(key);
		} else {
			int firstdot=key.indexOf('.');
			String first = key.substring(0, firstdot);
			String subKey=key.substring(firstdot+1);
			Object subModel = subModels.get(first);
			if (subModel==null) 
                return null;
            if (subModel instanceof Model)
                return ((Model)subModel).getAttribute(subKey);
            else {
        		String[] subPath = subKey.split("\\.");
        		for (int i=0; i<subPath.length-1;i++) {
        			//if submodel sit list &&  subPath[i] is integer
        			// submodel = submodel.get(subPath[i])
//        			int x = 
        			if(subModel instanceof List && Character.isDigit(subPath[i].charAt(0))){
        				List list = (List) subModel;
        				int index = Integer.parseInt(subPath[i]);
//        				if(index<list.size())
        					subModel = list.get(index);
//        				else
//        					return null;
        			} else {
        				subModel = Pojo.get(subModel, subPath[i]);
        			}
        			if (subModel == null)
        				return null;
        		}
        		if(subModel instanceof List && Character.isDigit(subPath[subPath.length-1].charAt(0))){
        			return ((List)subModel).get(Integer.parseInt(subPath[subPath.length-1]));
        		} else {
        			return Pojo.get(subModel, subPath[subPath.length-1]);
        		}
            }
		}
	}
	/**
	 * sets an Attribute with regard to a possible hierarchically organized model structure
	 */
	public void setAttribute(String key, Object value) throws EntityException {
		removeDataChangedListenerIfNecessary(key);	
		if (key.indexOf('.')==-1){
            registerObject(key,value);
            fireDataChanged(new DataChangedEvent(this,key));
		} else {
			int firstdot=key.indexOf('.');
			String first = key.substring(0, firstdot);
			String subKey=key.substring(firstdot+1);
			if (subModels.containsKey(first)){
                Object subModel = subModels.get(first);
                if (subModel == null)
                    throw new RuntimeException("no submodel for key: '"+first +"' found");
                if (subModel instanceof Model)
                    ((Model)subModel).setAttribute(subKey, value);
                else {
            		String[] subPath = subKey.split("\\.");
            		int i;
            		for (i=0; i<subPath.length-1;i++){            			
            			if(subModel instanceof List && Character.isDigit(subPath[i].charAt(0))){
            				List list = (List) subModel;
            				int index = Integer.parseInt(subPath[i]);
            				subModel = list.get(index);
            			} else {
            				subModel = Pojo.get(subModel, subPath[i]);
            			}
            		}
            		if(subModel instanceof List){
            			List list = (List) subModel;
            			list.set(Integer.parseInt(subPath[subPath.length-1]), value);
            		} else {
            			Pojo.set(subModel, subPath[subPath.length-1], value);
            		}
                    fireDataChanged(new DataChangedEvent(this,key));
                }                 
            }
		}
	}
	
	public void dataChanged(DataChangedEvent e) {
		Object submodelPrefix = subModelPaths.get(e.getSource());
		if (submodelPrefix != null) {
			String path =  submodelPrefix +"."+e.getAttributePath();
			fireDataChanged(new DataChangedEvent(this,path));
		}
		// else: do not push the event
	}

	//----- getters & setters ....

	public Model getModel(String key){
		return (Model)subModels.get(key);
	}
	
	public HashMap getModels() {
		return subModels;
	}
	
	public void registerModel(String key, Model model){
		removeDataChangedListenerIfNecessary(key);	
		model.addDataChangedListener(this);
		subModels.put(key,model);
		subModelPaths.put(model, key);
	}
	public void deregisterModel(String key){
		removeDataChangedListenerIfNecessary(key);	
		Object model = subModels.remove(key);
		subModelPaths.remove(model);
	}
	
	public EntityList getEntityList(String key){
		return (EntityList)subModels.get(key);
	}
	public void registerEntityList(String key,EntityList list){
		if (list instanceof DataSubject)
			((DataSubject)list).addDataChangedListener(this);
		removeDataChangedListenerIfNecessary(key);	
		subModels.put(key, list);
		subModelPaths.put(list, key);
	}
	
	public void deregisterEntityList(String key){
		removeDataChangedListenerIfNecessary(key);	
		Object model = subModels.remove(key);
		subModelPaths.remove(model);
	}
	
	public Object getObject(String key){
		return subModels.get(key);
	}
	
	public void registerObject(String key,Object obj){
		if (obj instanceof DataSubject)
			((DataSubject)obj).addDataChangedListener(this);
		removeDataChangedListenerIfNecessary(key);	
		subModels.put(key, obj);
		subModelPaths.put(obj, key);
	}
	
	public void deregisterObject(String key){
		removeDataChangedListenerIfNecessary(key);	
		Object model = subModels.remove(key);
		subModelPaths.remove(model);
	}
	
	private void removeDataChangedListenerIfNecessary(String key){
		if ((subModels.containsKey(key))&&(subModels.get(key)instanceof DataSubject)){
			((DataSubject)subModels.get(key)).removeDataChangedListener(this);
		}
	}
}
