/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

/**
 * 
 */
package de.tarent.commons.fileformats;

import javax.swing.ImageIcon;
import javax.swing.filechooser.FileFilter;

/**
 * @author Fabian K�ster (f.koester@tarent.de), tarent GmbH Bonn
 *
 */
public interface FileFormat
{
	public String getKey();
	public void setKey(String pKey);
	public String getDescription();
	public void setDescription(String description);
	public ImageIcon getIcon();
	public void setIcon(ImageIcon icon);
	public String getLongName();
	public void setLongName(String longName);
	public String getShortName();
	public void setShortName(String shortName);
	public String getSuffix(int index);
	public void addSuffix(String suffix);
	public boolean endsWithSuffix(String filename);
	public int getNumberOfSuffixes();  
	public FileFilter getFileFilter();
	public boolean canLoad();
	public boolean canSave();  
	public void setCanLoad(boolean canLoad);
	public void setCanSave(boolean canSave);
	public boolean isTemplateFormat();
	public boolean isXMLFormat();
	public void setTemplateFormat(boolean pTemplate);
	public void setXMLFormat(boolean pXML);
}
