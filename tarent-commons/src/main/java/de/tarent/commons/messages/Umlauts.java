/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.commons.messages;

public class Umlauts {
	/** Small german ae. */
	public static final String ae = "\u00e4";

	/** Large german ae. */
	public static final String AE = "\u00c4";

	/** Small german ue. */
	public static final String ue = "\u00fc";

	/** Large german ue. */
	public static final String UE = "\u00dc";

	/** Small german oe. */
	public static final String oe = "\u00f6";

	/** Large german oe. */
	public static final String OE = "\u00d6";

	/** German ss. */
	public static final String ss = "\u00df";

	/** Slash. */
	public static final String SLASH = "\u002f";

	/** Backslash. */
	public static final String BACKSLASH = "\u005c\u005c";

	/** Apostroph. */
	public static final String APOS = "\u0027";

	/** Quoting. */
	public static final String QUOT = "\u005c\u0022";

	/** And */
	public static final String AMP = "\u0026", AND = AMP;

	/** Lower than. */
	public static final String LT = "\u003c";

	/** Greater than. */
	public static final String GT = "\u003e";
}
