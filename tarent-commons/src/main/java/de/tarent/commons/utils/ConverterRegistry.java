/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.commons.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tarent.commons.utils.converter.BooleanToInteger;
import de.tarent.commons.utils.converter.BooleanToString;
import de.tarent.commons.utils.converter.CalendarToDate;
import de.tarent.commons.utils.converter.ClobToComplexType;
import de.tarent.commons.utils.converter.ClobToString;
import de.tarent.commons.utils.converter.ComplexTypeToClob;
import de.tarent.commons.utils.converter.DateToLong;
import de.tarent.commons.utils.converter.DateToString;
import de.tarent.commons.utils.converter.LongToDate;
import de.tarent.commons.utils.converter.NumberToBoolean;
import de.tarent.commons.utils.converter.NumberToInteger;
import de.tarent.commons.utils.converter.NumberToLong;
import de.tarent.commons.utils.converter.ObjectToString;
import de.tarent.commons.utils.converter.StringToBoolean;
import de.tarent.commons.utils.converter.StringToCharacter;
import de.tarent.commons.utils.converter.StringToDate;
import de.tarent.commons.utils.converter.StringToDimension;
import de.tarent.commons.utils.converter.StringToDouble;
import de.tarent.commons.utils.converter.StringToFloat;
import de.tarent.commons.utils.converter.StringToInteger;
import de.tarent.commons.utils.converter.StringToLong;
import de.tarent.commons.utils.converter.XMLDecoderConverter;
import de.tarent.commons.utils.converter.XMLEncoderConverter;

/**
 * This is a registry for converter of data items using the converter interface.
 * A default set of converter is provided out of the box. Other converter can 
 * be added.
 */
public class ConverterRegistry {
    private static ConverterRegistry defaultRegistry = null;


    /** The default converter in a list */
    private LinkedList converterList = new LinkedList();

    /** All converter by there name */
    private Map converterMap = new HashMap();

    private ConverterRegistry() {
    }
    
    public static ConverterRegistry createEmptyRegistry() {
    	return new ConverterRegistry();
    }
    
    public static ConverterRegistry createPrefilledRegistry() {
    	ConverterRegistry registry = new ConverterRegistry();
    	
    	registry.register(new ObjectToString(), true);
        
    	registry.register(new XMLEncoderConverter(Map.class), true); //MapToString
    	registry.register(new XMLEncoderConverter(List.class), true); //MapToString
    	registry.register(new XMLDecoderConverter(Map.class), true); //StringToMap
    	registry.register(new XMLDecoderConverter(List.class), true); //StringToList
    	registry.register(new ClobToString(), true);
    	registry.register(new StringToDate(), true);
    	registry.register(new CalendarToDate(), true);
    	registry.register(new LongToDate(), true);
    	registry.register(new DateToLong(), true);
    	registry.register(new DateToString(), true);   	
    	registry.register(new StringToLong(), true);
    	registry.register(new StringToFloat(), true);
    	registry.register(new StringToDouble(), true);
    	registry.register(new StringToDimension(), true);
    	registry.register(new StringToCharacter(), true);
    	registry.register(new BooleanToString(), true);
    	registry.register(new BooleanToInteger(), true);
    	registry.register(new NumberToBoolean(), true);
    	registry.register(new NumberToLong(), true);
    	registry.register(new NumberToInteger(), true);
    	registry.register(new StringToBoolean(), true);
    	registry.register(new StringToInteger(), true);
    	registry.register(new ClobToComplexType(Map.class), true);
    	registry.register(new ComplexTypeToClob(Map.class), true);
    	registry.register(new ClobToComplexType(List.class), true);
    	registry.register(new ComplexTypeToClob(List.class), true);

    	return registry;
    }

    public static synchronized ConverterRegistry getDefaultRegistry() {
        if (defaultRegistry == null) {
            defaultRegistry = createPrefilledRegistry();
        }
        return defaultRegistry;
    }

    /**
     * Quick method for conversion over the registered converters of the default registry.
     * If the sourceData is Null and the target is a primitive, a default value is returned.
     * In the case, where the sourceData is directly assigneable to the targetType, no conversion will be done.
     *
     * @throws IllegalArgumentException if no converter was found, or the conversion fails.
     */
    public static Object convert(Object sourceData, Class targetType) throws IllegalArgumentException {
        if (sourceData == null) {
            if (targetType.isPrimitive())
                return getNullDefault(targetType);
            return null;
        }
        if (targetType.isPrimitive())
            targetType = getComplexForPrimitive(targetType);            
        
        if (targetType.isAssignableFrom(sourceData.getClass()))
            return sourceData;
        Converter converter = ConverterRegistry.getDefaultRegistry().getConverter(sourceData.getClass(), targetType);
        if (converter == null)
            throw new IllegalArgumentException("No suitable converter for "+ sourceData.getClass() +" => "+ targetType);
        return converter.convert(sourceData);
    }

    /**
     * Quick method for conversion over the registered converters of the default registry.
     * If the sourceData is Null and the target is a primitive, a default value is returned.
     * In the case, where the sourceData is directly assigneable to the targetType, no conversion will be done.
     *
     * @throws IllegalArgumentException if no converter was found, or the conversion fails.
     */
    public static Object convert(Object sourceData, Class targetType, Converter converter) {
        if (sourceData == null) {
            if (targetType.isPrimitive())
                return getNullDefault(targetType);
            return null;
        }
        if (targetType.isPrimitive())
            targetType = getComplexForPrimitive(targetType);            
        
        if (targetType.isAssignableFrom(sourceData.getClass()))
            return sourceData;
        
        if (converter == null)
            throw new IllegalArgumentException("No suitable converter for "+ sourceData.getClass() +" => "+ targetType);
        return converter.convert(sourceData);
    }

    /**
     * Quick method for conversion over the registered converters of the default registry.
     */
    public static Object convert(Object sourceData, String converterName) throws IllegalArgumentException {
        if (sourceData == null)
            return null;
        Converter converter = ConverterRegistry.getDefaultRegistry().getConverter(converterName);
        if (converter == null)
            throw new IllegalArgumentException("No suitable converter for name "+ converterName);
        return converter.convert(sourceData);
    }
    
    /**
     * Add a new converter to the Registry. Later registered converters are preferred by the lookup via type.
     * If the flag useByDefault is true, the converter is returned for a query by datatype,
     * otherwise it is only registered by name.
     *
     * @param useByDefault use the supplied converter by default
     */
    public void register(Converter converter, boolean useByDefault) {
        if (useByDefault)       
            converterList.addFirst(converter);
        converterMap.put(converter.getConverterName(), converter);
    }

    /**
     * Returns a converter for convertion from sourceType to targetType.
     * The first converter matching the data types is returned.
     * If you need a special converter, use the getConverter(String converterName).
     */
    public Converter getConverter(Class sourceType, Class targetType) {
        if (targetType.isPrimitive())
            targetType = getComplexForPrimitive(targetType);

        for (Iterator iter = converterList.iterator(); iter.hasNext();) {
            Converter converter = (Converter)iter.next();
            if (targetType.isAssignableFrom(converter.getTargetType())
                && converter.getSourceType().isAssignableFrom(sourceType))      
                return converter;
        }
        return null;
    }

    public static Class getComplexForPrimitive(Class targetType) {
        if (targetType == Boolean.TYPE)
            return Boolean.class;

        else if (targetType == Character.TYPE)
            return Character.class;

        else if (targetType == Byte.TYPE)
            return Byte.class;

        else if (targetType == Short.TYPE)
            return Short.class;

        else if (targetType == Integer.TYPE)
            return Integer.class;

        else if (targetType == Long.TYPE)
            return Long.class;

        else if (targetType == Float.TYPE)
            return Float.class;
        
        else if (targetType == Double.TYPE)
            return Double.class;
        
        else
            return targetType;
    }

    /**
     * Returns a default Value for primitives which best match a null value.
     */
    public static Object getNullDefault(Class targetType) {

        if (targetType == Boolean.TYPE)
            return Boolean.FALSE;

        else if (targetType == Character.TYPE)
            return new Character((char)0);

        else if (targetType == Byte.TYPE)
            return new Byte((byte)0);

        else if (targetType == Short.TYPE)
            return new Short((short)0);

        else if (targetType == Integer.TYPE)
            return new Integer(0);

        else if (targetType == Long.TYPE)
            return new Long(0);

        else if (targetType == Float.TYPE)
            return new Float(0);

        else if (targetType == Double.TYPE)
            return new Double(0);

        return null;
    }


    
    /**
     * Returns a special named converter.
     */
    public Converter getConverter(String converterName) {
        return (Converter)converterMap.get(converterName);
    }   
}