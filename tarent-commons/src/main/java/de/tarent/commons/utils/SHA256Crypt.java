/* $Id$ */

/*-
 * Unix crypt implementation using eglibc’s extension for SHA-256
 *
 * Copyright © 2010
 *	Thorsten Glaser <t.glaser@tarent.de>
 *
 * This code is derived from DrFTPD, Distributed FTP Daemon.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This code has been adopted for Director by © 2004
 *	Thomas Aeby
 * This code is based on the work of © 1999
 *	Jonathan Abbey <jonabbey@arlut.utexas.edu>
 * Ganymede Directory Management System © 1996, 1997, 1998, 1999
 *	The University of Texas at Austin.
 * Parts derived from C code written for The MirOS Project © 2010
 *	Thorsten Glaser <tg@mirbsd.org>
 * Parts derived from the GNU glibc version © 2007, 2009
 *	Free Software Foundation, Inc.
 * Contributed by © 2007
 *	Ulrich Drepper <drepper@redhat.com>
 *
 * This code is based on the C version under the following licence:
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <phk@login.dknet.dk> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Poul-Henning Kamp
 * ----------------------------------------------------------------------------
 */

package org.evolvis.util;

public final class SHA256Crypt {

/* private slots */

static private final String digits =
    "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
static private final String magic = "$5$";
static private final String rounds_prefix = "rounds=";
static private final int saltlen_max = 16;
static private final int mdlen = 32;
static private final int rounds_def = 5000;
static private final int rounds_min = 1000;
static private final int rounds_max = 999999999;

/* private methods */

/**
 * convert an encoded unsigned byte value
 * into an int with the unsigned value
 */
static private final int
bytes2u(byte c)
{
	return ((int)(c & 0xFF));
}

/**
 * clear internal state for security
 */
static private final void
clearbits(byte bits[])
{
	for (int i = 0; i < bits.length; ++i)
		bits[i] = 0;
}

/**
 * convert a 24-bit (3 byte) value into base64-like up to 4 bytes
 */
static private final String
crypt_24to64(byte v1, byte v2, byte v3, int n)
{
	long v;
	StringBuffer result = new StringBuffer();

	v = (bytes2u(v1) << 16) | (bytes2u(v2) << 8) | bytes2u(v3);
	while (--n >= 0) {
		result.append(digits.charAt((int)(v & 0x3F)));
		v >>>= 6;
	}

	return (result.toString());
}

/* public methods */

/**
 * Testcode for the command line
 */
static public void
main(String argv[])
{
	String salt /* initialise to please javac warnings */ = "";

	if (argv.length == 1)
		salt = gensalt();
	else if (argv.length == 2)
		salt = argv[1];
	else {
		System.err.println("Usage: SHA256Crypt 'password' ['salt']");
		System.exit(1);
	}

	System.out.println(crypt(argv[0], salt));
	System.exit(0);
}

/**
 * Generate a random salt using java.util.Random
 */
static public final String
gensalt()
{
	StringBuffer salt = new StringBuffer();
	java.util.Random randgen = new java.util.Random();

	while (salt.length() < 16) {
		salt.append(digits.charAt((int)(randgen.nextFloat() *
		    digits.length())));
	}

	return (salt.toString());
}

/**
 * Create a Unix crypt hash from the password and salt arguments
 * using the eglibc SHA-256 extension format
 */
static public final String
crypt(String password, String salt)
{
	int cnt, cnt2;
	int rounds = rounds_def;
	boolean rounds_custom = false;
	byte md[];
	byte tmp[];
	byte key[];
	byte slt[];
	byte[] p_bytes = null;
	byte[] s_bytes = null;
	java.security.MessageDigest ctx;

	/* get a MessageDigest instance */
	try {
		ctx = java.security.MessageDigest.getInstance("SHA-256");
	} catch (java.security.NoSuchAlgorithmException ex) {
		throw new RuntimeException(ex);
	}

	/* refine the Salt first */

	/* if it starts with the magic string, then skip that */
	if (salt.startsWith(magic))
		salt = salt.substring(magic.length());
	if (salt.startsWith(rounds_prefix)) {
		String arg = salt.substring(rounds_prefix.length(),
		    salt.indexOf('$'));
		cnt = Integer.valueOf(arg).intValue();
		salt = salt.substring(salt.indexOf('$') + 1);
		rounds = Math.max(rounds_min, Math.min(rounds_max, cnt));
		rounds_custom = true;
	}
	/* it stops at the first '$', max saltlen_max bytes */
	if (salt.indexOf('$') != -1)
		salt = salt.substring(0, salt.indexOf('$'));
	if (salt.length() > saltlen_max)
		salt = salt.substring(0, saltlen_max);

	/* for easier access */
	key = password.getBytes();
	slt = salt.getBytes();

	/* prepare for later: md = hash(password + salt + password) */
	ctx.reset();
	ctx.update(key);
	ctx.update(slt);
	ctx.update(key);
	md = ctx.digest();

	/* now hash the input (password, salt) */
	ctx.reset();
	ctx.update(key);
	ctx.update(slt);

	/* then just as many characters of the MD(pw+salt+pw) */
	for (cnt = key.length; cnt > mdlen; cnt -= mdlen)
		ctx.update(md, 0, mdlen);
	ctx.update(md, 0, cnt);

	/*
	 * Take the binary representation of the length of the key and
	 * for every 1 add the MD(pw+salt+pw), for every 0 the pw.
	 */
	for (cnt = key.length; cnt != 0; cnt >>>= 1)
		if ((cnt & 1) != 0)
			ctx.update(md, 0, mdlen);
		else
			ctx.update(key);

	/* create intermediate result */
	md = ctx.digest();

	/* create the p byte sequence */

	/* for every byte in the password hash the entire password */
	ctx.reset();
	for (cnt = key.length; cnt > 0; --cnt)
		ctx.update(key);
	tmp = ctx.digest();

	p_bytes = new byte[key.length];
	for (cnt2 = 0, cnt = p_bytes.length; cnt >= mdlen; cnt -= mdlen) {
		System.arraycopy(tmp, 0, p_bytes, cnt2, mdlen);
		cnt2 += mdlen;
	}
	System.arraycopy(tmp, 0, p_bytes, cnt2, cnt);

	/* create the s byte sequence */

	ctx.reset();
	for (cnt = 0; cnt < 16 + bytes2u(md[0]); ++cnt)
		ctx.update(slt);
	tmp = ctx.digest();

	s_bytes = new byte[slt.length];
	for (cnt2 = 0, cnt = s_bytes.length; cnt >= mdlen; cnt -= mdlen) {
		System.arraycopy(tmp, 0, s_bytes, cnt2, mdlen);
		cnt2 += mdlen;
	}
	System.arraycopy(tmp, 0, s_bytes, cnt2, cnt);

	/* clear temporary byte array (we need a padding NUL later anyway) */
	clearbits(tmp);

	/*
	 * Repeatedly run the collected hash value through SHA256
	 * to burn CPU cycles.
	 */
	for (cnt = 0; cnt < rounds; ++cnt) {
		ctx.reset();
		if ((cnt & 1) != 0)
			ctx.update(p_bytes);
		else
			ctx.update(md, 0, mdlen);
		if ((cnt % 3) != 0)
			ctx.update(s_bytes);
		if ((cnt % 7) != 0)
			ctx.update(p_bytes);
		if ((cnt & 1) != 0)
			ctx.update(md, 0, mdlen);
		else
			ctx.update(p_bytes);
		md = ctx.digest();
	}

	/* Now make the output string */

	StringBuffer result = new StringBuffer();
	result.append(magic);
	if (rounds_custom) {
		result.append(rounds_prefix);
		result.append(rounds);
		result.append("$");
	}
	result.append(salt);
	result.append("$");

	result.append(crypt_24to64(md[0],  md[10], md[20], 4));
	result.append(crypt_24to64(md[21], md[1],  md[11], 4));
	result.append(crypt_24to64(md[12], md[22], md[2] , 4));
	result.append(crypt_24to64(md[3],  md[13], md[23], 4));
	result.append(crypt_24to64(md[24], md[4],  md[14], 4));
	result.append(crypt_24to64(md[15], md[25], md[5] , 4));
	result.append(crypt_24to64(md[6],  md[16], md[26], 4));
	result.append(crypt_24to64(md[27], md[7],  md[17], 4));
	result.append(crypt_24to64(md[18], md[28], md[8] , 4));
	result.append(crypt_24to64(md[9],  md[19], md[29], 4));
	result.append(crypt_24to64(tmp[0], md[31], md[30], 3));

	/* clear intermediates (security!) */
	ctx.reset();
	clearbits(md);
	clearbits(key);
	clearbits(slt);
	clearbits(p_bytes);
	clearbits(s_bytes);

	return (result.toString());
}

}
