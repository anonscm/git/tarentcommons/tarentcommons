/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.commons.utils.converter;

import java.beans.XMLDecoder;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Clob;
import java.sql.SQLException;

import de.tarent.commons.utils.AbstractConverter;

/**
 * Converts from Clob to an Java object, using the Java XMLEndoder API.
 */
public class ClobToComplexType extends AbstractConverter {

    Class targetClass;

    public ClobToComplexType(Class targetClass) {
        this.targetClass = targetClass;
    }
    
    /**
     * Returns the Name of the converter
     */
    public String getConverterName() {
    	return "ClobTo"+getLocalClassName(getTargetType())+"(ClobToComplexType)";
    }
        
    public Class getTargetType() {
        return targetClass;
    }
    public Class getSourceType() {
        return Clob.class;
    }
    
    public Object doConversion(Object sourceData) throws IllegalArgumentException, SQLException, IOException {
        if (sourceData == null || "".equals(sourceData))
            return null;

        if (! (sourceData instanceof Clob)) 
            throw new IllegalArgumentException("Source data is not instance of Clob ("+sourceData.getClass() +")");
        
        //###### Clob to String
        StringBuffer strOut = new StringBuffer();
        String aux;

        BufferedReader br = new BufferedReader(((Clob) sourceData)
        		.getCharacterStream());
        while ((aux = br.readLine()) != null)
        	strOut.append(aux);

        String clobString =  strOut.toString();
  
        //####### String to XML
		XMLDecoder decoder = new XMLDecoder(new ByteArrayInputStream((clobString).getBytes()));
        Object result = decoder.readObject();
        decoder.close();
        
        if (result != null && ! targetClass.isAssignableFrom(result.getClass()))
            throw new IllegalArgumentException("Decoded type is not instance of "+targetClass+" ("+result.getClass() +")");
        
        return result;
    } 
}   
