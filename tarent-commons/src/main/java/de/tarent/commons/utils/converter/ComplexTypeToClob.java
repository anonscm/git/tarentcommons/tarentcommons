/*
 * tarent commons,
 * a set of common components and solutions
 * Copyright (c) 2006-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'tarent commons'
 * Signature of Elmar Geese, 14 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.commons.utils.converter;

import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Clob;

import de.tarent.commons.utils.AbstractConverter;

/**
 * Converts from an Java object to Clob, using the Java XMLEndoder API.
 */
public class ComplexTypeToClob extends AbstractConverter {

    Class sourceClass;

    public ComplexTypeToClob(Class sourceClass) {
        this.sourceClass = sourceClass;
    }

    /**
     * Returns the Name of the converter
     */
    public String getConverterName() {
    	return getLocalClassName(getSourceType()) +"ToClob(ComplexTypeToClob)";
    }
    
    public Class getTargetType() {
        return Clob.class;
    }

    public Class getSourceType() {
        return sourceClass;
    }
    
    public Object doConversion(Object sourceData) throws IllegalArgumentException {
        if (sourceData != null && !sourceClass.isAssignableFrom(sourceData.getClass())) 
            throw new IllegalArgumentException("Source data is not instance of "+sourceClass+" ("+sourceData.getClass() +")");

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        XMLEncoder encoder = new XMLEncoder(bos);
        encoder.writeObject(sourceData);
        encoder.close();
        
        try {
            return bos.toString("UTF-8");
        } catch (UnsupportedEncodingException e) { // should never occur with UTF-8
            throw new RuntimeException(e); 
        }
    } 
  
}   
